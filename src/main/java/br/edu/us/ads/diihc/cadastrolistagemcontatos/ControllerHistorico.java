package br.edu.us.ads.diihc.cadastrolistagemcontatos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller /*Indica que uma classe anotada é um "Controlador" (por exemplo, um controlador da Web).
Essa anotação serve como uma especialização de @Component, permitindo que as classes de implementação sejam detectadas automaticamente por meio da varredura do caminho de classe. É normalmente usado em combinação com métodos de manipulador anotados com base na RequestMappinganotação.*/

public class ControllerHistorico {

    

    @Autowired /*A anotação @ Autowired fornece controle sobre onde e como a ligação entre os beans deve ser realizada. Pode ser usado para em métodos setter, no construtor, em uma propriedade ou métodos com nomes arbitrários e / ou vários argumentos, ou seja, as anotações @Autowired permitem que o Spring injete as dependências nesta classe*/

    OperacaoRepository operacaoRepository; /*Anotação que serve para definir uma classe como pertencente à camada de persistência.*/


////// ROTAS GET @GetMappinga anotação mapeia solicitações HTTP GET para métodos específicos do manipulador. É uma anotação composta que atua como um atalho para @RequestMapping(method = RequestMethod.GET) //////
    @GetMapping(value = "/")
    public ModelAndView getDefaut() {

        ModelAndView modelAndView = new ModelAndView("index"); // Criando um construtor chamando o templates/index

        return modelAndView;

    }

    @GetMapping(value = "/visualizar")
    public ModelAndView getVisualizar() {

        ModelAndView modelAndView = new ModelAndView("visualizar"); // Criando um construtor chamando o templates/index
        modelAndView.addObject("mensagem", " Cadastrado com Sucesso");
        modelAndView.addObject("historico", operacaoRepository.findAll());
        return modelAndView;

    }



    @GetMapping(value = "/cadastro")
    public ModelAndView getCadastro() {
        Contato contato = new Contato();

        ModelAndView modelAndView = new ModelAndView("cadastro"); // Criando um construtor chamando o templates/index
        modelAndView.addObject("contato", contato);
        return modelAndView;

    }



    @GetMapping(value = "/deleta/{id}")
    public String getDeleta(@PathVariable Long id) {
        operacaoRepository.deleteById(id);
    
        return "redirect:/visualizar";
    }

    @GetMapping(value = "/deleta/visualizar")
    public ModelAndView getDeletar() {
        
        ModelAndView modelAndView = new ModelAndView("visualizar"); // Criando um construtor chamando o templates/index
        modelAndView.addObject("historico", operacaoRepository.findAll());
        return modelAndView;
    }

    @GetMapping(value = "/editar/{id}")
    public ModelAndView getEditar(@PathVariable Long id) {
        // retornar o formulario com a bedida ID preenchida no form
        Contato contato = operacaoRepository.findById(id).get();
        ModelAndView modelAndView = new ModelAndView("cadastro");
        modelAndView.addObject("contato", contato);
        return modelAndView;
    }

    /*@GetMapping(value = "/deleta/cadastro")
    public ModelAndView getDeletar2() {
        
        ModelAndView modelAndView = new ModelAndView("cadastro"); // Criando um construtor chamando o templates/index
        return modelAndView;
    }
*/

    /////////////ROTAS POST  @PostMappinga anotação mapeia solicitações HTTP POST para métodos específicos do manipulador. É uma anotação composta que atua como um atalho para @RequestMapping(method = RequestMethod.POST)./////
    @PostMapping(value = "/cadastro") 
    public ModelAndView postCadastro(Contato contato) {

         System.out.println("Passei por aqui");
     
        operacaoRepository.save(contato);

        ModelAndView modelAndView = new ModelAndView("visualizar"); // Criando um construtor chamando o templates/index

        //modelAndView.addObject("mensagem", " Cadastrado com Sucesso");
        modelAndView.addObject("historico", operacaoRepository.findAll());


        return modelAndView;

    }

    




}