package br.edu.us.ads.diihc.cadastrolistagemcontatos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Conexao {

	public static void main(String[] args) {
		SpringApplication.run(Conexao.class, args);
	}

}
