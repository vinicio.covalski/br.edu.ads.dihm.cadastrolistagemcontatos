package br.edu.us.ads.diihc.cadastrolistagemcontatos;


import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface OperacaoRepository extends CrudRepository<Contato,Long> {
  List<Contato> findAll();

} 
    
